import datetime
import os
from datetime import timedelta

def getStrTimeNow(format):
    now = datetime.now()

    now = now + timedelta(hours=0)
    if format == 'HMS' or format == 'hms':
        return datetime.datetime.now().strftime("%H:%M:%S")
    elif format == 'MS' :
        return datetime.datetime.now().strftime("%M:%S")
    elif format == 'DMYHMS':
        return now.strftime("%d.%m.%Y %H:%M")
    elif format == 'YMDHMS':
        return now.strftime("%Y%m%d_%H%M%S")


from datetime import datetime, timezone


def get1pid():
    return str(os.getpid())[-1]