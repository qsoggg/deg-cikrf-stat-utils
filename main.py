import requests
from fake_useragent import UserAgent
import os
import time
from utils import getStrTimeNow

# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
url = 'https://stat.vybory.gov.ru/api/'

token = ''

headers = { 'User-Agent': UserAgent().random}

# Press the green button in the gutter to run the script.
def write_json(election_path, json):
    f = open(election_path+'/'+'info.txt', 'w')
    f.write(str(json))
    f.close()
    pass

def check_token():
    f = open('bearer.token', 'r')
    token = f.read()
    f.close()

    # headers[ "Authorization"]  = "Bearer "+ token
    responce = requests.get(url + 'voting/regions', headers=headers)
    if 'error' in responce.json():
        print(responce.json())
        if responce.json()['error']['code'] == 10:
            print('new token request')
            #query
            #write new token in file
    else:
        return True
    print(responce.json())
    return False
    pass


if __name__ == '__main__':


    if check_token():


        start_time = time.time()


        response = requests.get(url=url, headers=headers)

        responce = requests.get(url + 'voting/regions', headers=headers)

        print(headers)

        # создаем папку текущего состояния - дата

        current_path = getStrTimeNow('YMDHMS')
        os.mkdir(current_path)
        if 'data' in responce.json():
            regions = responce.json()["data"]["values"]
            for region in regions:

                #Создаем папку суюъекта
                region_path = current_path + '/' + str(region['code'])
                os.mkdir(region_path)
                params = {'regionCode': region['code']}
                responce = requests.get(url + 'elections', headers=headers, params = params)
                if 'data' in responce.json():
                    election_levels = responce.json()["data"]['elections']
                    # print(campaigns)
                    for election_level in election_levels:
                        for election in election_level['elections']:
                            # создаём папку кампании
                            # описание кампании
                            # print(election)
                            election_path = region_path + '/'+ election['electionId']
                            os.mkdir(election_path)
                            write_json(election_path, election)

                            params = {'regionCode': region['code'], 'electionId':election['electionId']}
                            responce = requests.get(url + 'elections/districts', headers=headers, params=params)
                            if 'data' in responce.json():
                                districts = responce.json()['data']

                                # \ округ
                                # \ описание округа
                                for district in districts:
                                    district_path = election_path + '/' + district['id']
                                    os.mkdir(district_path)
                                    write_json(district_path, district)
                                    # print(district)
                                    params = {'districtId': district['id']}
                                    responce = requests.get(url + 'statistics/voting', headers=headers, params=params)
                                    # print(responce.json())
                                    if 'data' in responce.json():
                                        votings = responce.json()['data']['votings']
                                        for voting in votings:
                                            # print(voting)
                                            voting_path = district_path + '/' +  voting['counters']['contractId']
                                            os.mkdir(voting_path)
                                            write_json(voting_path,voting)
                                            uik_path = voting_path + '/' + str(time.time())
                                            os.mkdir(uik_path)
                                            responce = requests.get(url + 'statistics/voting/' + voting['counters']['contractId']+'/uiks', headers=headers)
                                            if 'data' in responce.json():
                                                uiks = responce.json()['data']
                                                write_json(uik_path, uiks)
                                                # print(uiks)


        time = time.time()-start_time
        print('Time:', time, 'sec')

                # single
                # УИКИ single тазвание файла - таймстемп
                # ns signle
                # УИКИ (тазвание файла - таймстемп)

                # responce = requests.get(url + 'elections?regionCode=' + str(region['code']), headers=headers)
                # if 'data' in responce.json():
                #     compaings = responce.json()["data"]
                #     print(compaings)




    # See PyCharm help at https://www.jetbrains.com/help/pycharm/
