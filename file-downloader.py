# Приложение обеспечивает выгрузку файлов с портала наблюдения stat.vybory.gov.ru
#
# для запуска необходимо установить python3
# после запуска выполнить команду pip install fake_useragent
#
# для запуска необходим bearer токен наблюдателя ЕСИА, токен нужно разместить в файле token.txt
# токен хранится в Chrome: Developer tools -> Application -> local storage -> https://stat.vybory.gov.ru -> token
#
# скрипт запускается командой python3 file-downloader.py
#
# Выбор региона для скачивания: укажите коды субъектов параметром в строке через пробел:
# Например для Чувашии и Владимирской области:
# python3 file-downloader.py 21 33
#


import requests
from fake_user_agent import user_agent
import os
import time
from utils import getStrTimeNow
import urllib.request
from auth_data import cookie
import sys

url = 'https://stat.vybory.gov.ru/api/'

token = ''

headers = { 'User-Agent': user_agent("chrome")}

# Press the green button in the gutter to run the script.
def write_json(election_path, json):
    f = open(election_path+'/'+'info.txt', 'w')
    f.write(str(json))
    f.close()
    pass

def check_token():
    f = open('bearer.token', 'r')
    token = f.read()
    f.close()

    headers[ "Authorization"]  = "Bearer "+ token
    responce = requests.get(url + 'voting/regions', headers=headers, cookies=cookie())
    if 'error' in responce.json():
        print(responce.json())
        if responce.json()['error']['code'] == 10:
            print('new token request')
            #query
            #write new token in file
    else:
        print ('token ok')
        return True
    print(responce.json())
    return False
    pass


if __name__ == '__main__':
    reg_list = []
    if len(sys.argv) > 1:
        for arg in sys.argv:
            reg_list.append(arg)
        reg_list.pop(0)


    if check_token():


        start_time = time.time()

        sess = requests.Session()
        sess.headers.update(headers)
        response = sess.get(url=url, cookies=cookie())


        # response = requests.get(url=url, headers=headers)
        try:
            responce = sess.get(url + 'voting/regions', headers=headers, cookies=cookie())
        except:
            print('Exepction')
        # создаем папку текущего состояния - дата

        current_path = getStrTimeNow('YMDHMS')
        os.mkdir(current_path)
        if 'data' in responce.json():
            # print(responce.json()["data"]["regions"])

            regions = responce.json()["data"]["regions"]
            # print(regions)


            for region in regions:
                if len(reg_list) > 0:
                    if str(region['code']) in reg_list:
                        print(region['code'],'-' , region['description'])

                        #Создаем папку субъекта
                        # region_path = current_path + '/' + str(region['code'])
                        region_path = current_path + '/' + str(region['code']) + ' - ' + region['description']
                        os.mkdir(region_path)
                        # params = {'regionCode': region['code']}
                        try:
                            responce = sess.get(url + 'elections/region/' + str(region['code']) + '/elections', headers=headers,  cookies=cookie())
                        except:
                            print('Exepction')
                        if 'data' in responce.json():
                            # print(responce.json())
                            election_levels = responce.json()["data"]['elections']
                            # print(election_levels)
                            # print(campaigns)
                            for election_level in election_levels:
                                # print (election_level)
                                for election in election_level['elections']:
                                    print(election['electionName'])
                                    # создаём папку кампании
                                    # описание кампании
                                    # print(election)
                                    # election_path = region_path + '/'+ election['electionId']
                                    election_path = region_path + '/'+ election['electionName']
                                    os.mkdir(election_path)
                                    write_json(election_path, election)

                                    params = {'regionCode': region['code'], 'electionId':election['electionId']}
                                    try:
                                        responce = sess.get(url + 'elections/districts', headers=headers, params=params, cookies=cookie())
                                    except:
                                        print('Exepction')
                                    if 'data' in responce.json():
                                        districts = responce.json()['data']

                                        # \ округ
                                        # \ описание округа
                                        for district in districts:
                                            # print(district)
                                            print(district['name'])
                                            # district_path = election_path + '/' + district['id']
                                            district_path = election_path + '/' + district['name']
                                            os.mkdir(district_path)
                                            write_json(district_path, district)
                                            # print(district)
                                            params = {'electionId': election['electionId'], 'districtId': district['id']}
                                            # print(params)
                                            try:
                                                responce = sess.get(url + 'statistics/voting', headers=headers, params=params, cookies=cookie())
                                            except:
                                                print('Exepction')
                                            # print(responce.json())
                                            if 'data' in responce.json():
                                                votings = responce.json()['data']['votings']
                                                for voting in votings:
                                                    # print('VotingID', voting)
                                                    voting_path = district_path + '/' +  voting['counters']['contractId']
                                                    os.mkdir(voting_path)
                                                    # запрос списка файлов
                                                    try:
                                                        responce = sess.get(
                                                            url + 'files/' + voting['counters']['contractId'],
                                                            headers=headers, cookies = cookie())
                                                    except:
                                                        print('Exepction')
                                                    # print("============")
                                                    # print(responce.json())
                                                    if 'data' in responce.json():
                                                        # print(responce.json())
                                                        filenames = responce.json()['data']
                                                        for filename in filenames:
                                                            # print(filename)
                                                            download_link = url + 'files/' + filename + \
                                                                            '/contract/' + voting['counters']['contractId'] + '/download'



                                                            # print(download_link)
                                                            # urllib.request.urlretrieve(download_link, filename)
                                                            try:
                                                                r = sess.post(download_link, allow_redirects=True, headers=headers, cookies=cookie())
                                                            except:
                                                                print('Exepction')
                                                            open(voting_path + '/' + filename, 'wb').write(r.content)

                                                        # print(responce.json())

                                                    # https: // stat.vybory.gov.ru / api / files / FupHCKDt8qcPcLXBZCKyYmZnBRkTJmoXsPRUidRNkbDh
                                                    # загрузка файлов

                                    #                 write_json(voting_path,voting)
                                    #                 uik_path = voting_path + '/' + str(time.time())
                                    #                 os.mkdir(uik_path)

                                    #                     uiks = responce.json()['data']
                                    #                     write_json(uik_path, uiks)
                                    #                     # print(uiks)


        time = (time.time()-start_time) // 1
        print('Time:', time, 'sec')

